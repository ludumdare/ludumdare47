[center]

[font=res://game/fonts/game_font_16.tres][color=teal]OCTOBER 5th, 2020[/color][/font]

[font=res://game/fonts/game_font_64.tres]BREAK THE LOOP[/font]

a game created for ludumdare 47


thanks for playing my game. i hope you like it.

many thanks to my wife and kids who put up with me this weekend. special shout out to my son who joined the national guard and is currently at basic training. i hope they give you some time to play my game.

shout out to [color=yellow]rapidpunches[/color] at [url=https://opengameart.org/users/rapidpunches]opengameart.org[/url] for some of the [url=https://opengameart.org/content/game-of-the-unknown]music[/url] i used in the game

the scoring data is stored online using the great [color=yellow]dreamlo[/color] service.
[url=https://www.dreamlo.com/]https://www.dreamlo.com/[/url]

big thanks to the
[img]res://icon.png[/img]
[color=yellow]godot team[/color]
for their awesome tool

the source code for this game is available at [url=https://gitlab.com/ludumdare/ludumdare47]https://gitlab.com/ludumdare/ludumdare47[/url]

don't forget the rate the game. i will play and rate every every person who leaves feedback on my game.

cheers

[font=res://game/fonts/game_font_64.tres]
[wave]PAUL[/wave]
[/font]

[color=yellow]version history[/color]

v1.2.1
[font=res://game/fonts/game_font_24.tres]
- update ludum dare engine to fix resource bug
[/font]

v1.2.0
[font=res://game/fonts/game_font_24.tres]
- fix display  in high score table to show seconds (not milliseconds)
[/font]

v1.1.0
[font=res://game/fonts/game_font_24.tres]
- small tweaks to level 14 & 15 that affected by screenshake
[/font]

v1.0.0
[font=res://game/fonts/game_font_24.tres]
- jam release of the game
[/font]



SpockerDotNet LLC

[color=silver]LIVE LONG AND GAME[/color]


[/center]




