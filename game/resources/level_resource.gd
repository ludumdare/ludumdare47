class_name LevelResource
extends Resource

export var level : PackedScene
export var rotation_speed : float = 15.0
export var player_speed : float = 50.0
export var trail_speed : float = 75.0
