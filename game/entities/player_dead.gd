
extends Node2D

export var speed = 1

onready var p1 = $Sprite1
onready var p2 = $Sprite2
onready var p3 = $Sprite3

var dir1 = Vector2.UP
var dir2 = Vector2(-1, -1)
var dir3 = Vector2(1, 1)

func _process(delta):
	

	p1.position += speed * dir1 * delta
	p2.position += speed * dir2 * delta
	p3.position += speed * dir3 * delta
	
