extends Node

const validchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-=_+[]{}\\|:;/?,.<>"


const winner_text = "woah! you have the honor of joining an elite group of players that have managed to complete this game. enter your name and click the AWESOME button to show the weak players your accomplishment!"
const continue_text = "good job! you finished the game. sorry but only players who finished the game without continuing get the honor of having their name in the spotlight. get better!"

func _ready():
	DreamLo.connect("dreamlo_added_score", self, "_on_dreamlo_added_score")
	var _name = OS.get_environment("username")
	$Control/LineEdit.text = _name
	
#	if (Game.continued):
#		$Control/WinMessage.text = continue_text
#		$Control/HomeButton.visible = true
#	else:

	$Control/WinMessage.text = winner_text
	$Control/LineEdit.visible = true
	$Control/SubmitButton.visible = true
		


func _on_LineEdit_text_changed(new_text):
	var _text = ""
	for i in new_text:
		if (i in validchars):
			_text += i
			
	$Control/LineEdit.text = _text
	$Control/LineEdit.caret_position = 99
	

func _on_LineEdit_focus_entered():
	$Control/LineEdit.select_all()


func _on_SubmitButton_pressed():
	var _name = $Control/LineEdit.text
	var _seconds = int(rand_range(1, 1234567))
	print(_seconds)
	
	if (_name.length() > 0):
		DreamLo.add_score(_name, 0, Game.time)


func _on_dreamlo_added_score():
	SceneManager.transition_to(Ludum.settings.score_scene)
	


func _on_HomeButton_pressed():
	SceneManager.transition_to(Ludum.settings.score_scene)
