extends Node2D

func _process(delta):
	if (Input.is_action_just_pressed("ui_cancel")):
		SceneManager.transition_to(Ludum.settings.home_scene)

func _ready():
	$Node/MenuButton.grab_focus()
