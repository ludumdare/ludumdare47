extends Node2D

onready var score_list = $Node/ScoreList

export var score_panel : PackedScene 


func _process(delta):
	if (Input.is_action_just_pressed("ui_cancel")):
		SceneManager.transition_to(Ludum.settings.home_scene)

func _on_scores_loaded(scores):
	
	var _score_count = scores.size()
	
	for i in range(_score_count):
		var _score = scores[i]
		var _panel = score_panel.instance()
		var _ms = int(_score["seconds"]) / 1000.0
		var _min = float(_ms / 60)
		_panel.get_node("$Player").text = _score["name"]
		_panel.get_node("$Time").text = "%5.2f" % [_min] 
		score_list.add_child(_panel)
		
		


func _ready():
	
	DreamLo.connect("dreamlo_got_scores", self, "_on_scores_loaded")
	DreamLo.get_scores_by_seconds()
	$Node/MenuButton.grab_focus()
