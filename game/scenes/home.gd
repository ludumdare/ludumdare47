extends LudumScene

var break_speed = 500
var the_speed = 100
var loop_speed = 300
var trail_speed = 1000


var break_offset = 0
var the_offset = 0
var loop_offset = 0
var trail_offset = 0


func _process(delta):
	
	trail_offset += trail_speed * delta
	
	$Path2D/FollowBreak/Break.visible = true
	break_offset += break_speed * delta
	
	if (break_offset > 1000):
		$Path2D/FollowThe/The.visible = true
		the_offset += the_speed * delta
		
	
	if (break_offset > 2000):
		$Path2D/FollowLoop/Loop.visible = true
		loop_offset += loop_speed * delta
		
		
	$Path2D/FollowTrail.offset = trail_offset
	$Path2D/FollowBreak.offset = break_offset
	$Path2D/FollowThe.offset = the_offset
	$Path2D/FollowLoop.offset = loop_offset
	
	
func on_ready():
	
	Game.start_music()
	
	$Path2D/Version.text = Ludum.settings.long_version
	$LudumHome/Node/VBoxContainer/PlayButton.grab_focus()

	if (Game.level_test != ""):
		SceneManager.transition_to(Core.settings.play_scene)
