extends Node2D

onready var music = $TitleMusic

export var game_settings : String = "res://game_work/game.cfg"
export var dreamlo_settings : String = "res://game_work/dreamlo.cfg"
export var window_manager_settings : Resource

signal hit_trigger(count)
signal hit_player()
signal no_power()
signal pause()
signal resume()

# the players score in milliseconds
var score = 0
# the players time in milliseconds
var time = 0
var power = 0
var triggers = 0
var level = 0
var continued = false
var debug = false

var player_rotation_speed : float = 15.0
var player_rotation_variance : float = 3.0
var player_speed : float = 50.0
var trail_speed : float = 75.0
var level_folder : String = ""
var levels : Array
var level_data : ConfigFile
var level_test

func stop_music():
	music.volume_db = -80.0
	
func start_music():
	music.volume_db = -30.0

func _ready():
	
	start_music()
	
	Ludum.start(game_settings, GameSettingsResource.new())
	DreamLo.load_settings(dreamlo_settings)
	WindowManager.load(window_manager_settings)
	
	var _config = Configuration.new() as Configuration
	var _levels = _config.load_config("res://game/data/levels.cfg") as ConfigFile
	
	player_rotation_speed = _levels.get_value("default", "player_rotation_speed", 15.0)
	player_rotation_variance = _levels.get_value("default", "player_rotation_variance", 3.0)
	player_speed = _levels.get_value("default", "player_speed", 50.0)
	trail_speed = _levels.get_value("default", "trail_speed", 75.0)
	level_folder = _levels.get_value("default", "level_folder", "res://")
	level_test = _levels.get_value("default", "level_test", "level1_work")
	debug = _levels.get_value("default", "debug", "false")
	
	var _level_keys = _levels.get_section_keys("levels")
	
	levels = []
	
	for _lk in _level_keys:
		var _active = _levels.get_value("levels", _lk)
		if (_active == 1):
			levels.append(_lk)
	
	level_data = _levels

	if (! debug):
		Core.change_state(Ludum.GAME_STATE_INIT)
		
	if (level_test):
		Core.change_state(Ludum.GAME_STATE_PLAY)
		
