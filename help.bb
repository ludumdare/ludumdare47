[center]
[u]PILOT[/u]

your ship is stuck in an endless loop and you are spinning out of control. shoot the targets to escape each level, but only shoot them once.

how fast can you finish each level? how fast can you finish the entire game? see your name on the scores list for those awesome players who are the fastest.

!! GOOD LUCK !!

[u]CONTROLS[/u]

[color=yellow]SHOOT[/color]
left mouse button

[color=yellow]ESCAPE[/color]
pause game

[/center]
