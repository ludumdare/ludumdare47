extends Node2D

#export var rot_speed_normal : float = 2.0
#export var rot_speed_shooting : float = 1.0
#export var loop_speed : float = .002
#export var trail_speed : float = .01


#onready var level1 := preload("res://game_work/level1_work.tscn")
#onready var level2 := preload("res://game_work/level2_work.tscn")
#onready var level3 := preload("res://game_work/level3_work.tscn")
#onready var level4 := preload("res://game_work/level4_work.tscn")
#onready var level5 := preload("res://game_work/level5_work.tscn")
#onready var level6 := preload("res://game_work/level6_work.tscn")
#onready var level7 := preload("res://game_work/level7_work.tscn")
#onready var level8 := preload("res://game_work/level8_work.tscn")
#onready var level9 := preload("res://game_work/level9_work.tscn")
#onready var level10 := preload("res://game_work/level10_work.tscn")

onready var root := $Level
onready var player_s = preload("res://game_work/player_work.tscn")
onready var trail_s = preload("res://game_work/trail_work.tscn")
onready var portal_s = preload("res://game_work/portal_work.tscn")
onready var player_dead_s = preload("res://game/entities/player_dead.tscn")

onready var audio_win : AudioStreamPlayer2D = $Win
onready var audio_lose : AudioStreamPlayer2D = $Lose
onready var audio_game : AudioStreamPlayer2D = $Game
onready var audio_crash : AudioStreamPlayer2D = $Crash

onready var ui_game = $UI/Game
onready var ui_lose = $UI/Lose
onready var ui_win = $UI/Win
onready var ui_level = $UI/Level

var player_location = 0
var trail_location = 0
var trigger_location = 0
var follow
var trail_offset
var portal_offset
var trigger_path
var player : Area2D
var laser
var rot_speed = 0
var trail : Particles2D
var portal
var level_scene
var is_enabled = false 
var time_start = 0
var time_end = 0
var time_level = 0
var is_paused = false
var resume_game = false

var player_speed
var player_rotation_speed
var player_rotation_variance
var trail_speed


func _unhandled_input(event: InputEvent) -> void:
	
	if (! is_enabled):
		return
	
	if not event.is_action("fire_weapon"):
		return
		
	laser.is_casting = event.is_action_pressed("fire_weapon")


func _ready():
	
	Game.connect("hit_trigger", self, "_on_hit_trigger")
	Game.connect("hit_player", self, "_on_hit_player")
	Game.connect("no_power", self, "_on_no_power")
	
	_prepare_level()
	
	ui_game.visible = true
	$"UI/Game/$Debug".visible = Game.debug
	

func _prepare_level():
	
	Game.stop_music()
	
	var _level_name = Game.levels[Game.level-1]
	
	if (Game.level_test != ""):
		_level_name = Game.level_test
		
	Logger.debug("- next level is {0}".format([_level_name]))
	
	var _level_s = "{0}/{1}.tscn".format([Game.level_folder, _level_name])
	
	print(_level_s)
	if (!ResourceLoader.exists(_level_s)):
		Logger.warn("- missing level {0}".format([_level_s]))
		SceneManager.transition_to(Core.settings.home_scene)


	var level_scene_resource = load(_level_s) as Resource
	level_scene = level_scene_resource.instance()
	root.add_child(level_scene)
	
	player_speed = Game.player_speed
	player_rotation_speed = Game.player_rotation_speed
	player_rotation_variance = Game.player_rotation_variance
	trail_speed = Game.trail_speed
	
	var _level_section = Game.levels[Game.level-1]
	
	if (Game.level_data.has_section(_level_section)):
		
		if (Game.level_data.has_section_key(_level_section, "player_speed")):
			player_speed = Game.level_data.get_value(_level_section, "player_speed", Game.player_speed)
			
		if (Game.level_data.has_section_key(_level_section, "player_rotation_speed")):
			player_rotation_speed = Game.level_data.get_value(_level_section, "player_rotation_speed", Game.player_rotation_speed)
			
		if (Game.level_data.has_section_key(_level_section, "player_rotation_variance")):
			player_rotation_variance = Game.level_data.get_value(_level_section, "player_rotation_variance", Game.player_rotation_variance)
			
		if (Game.level_data.has_section_key(_level_section, "trail_speed")):
			trail_speed = Game.level_data.get_value(_level_section, "trail_speed", Game.trail_speed)

	var _loop = level_scene.get_node("Loop")
	follow = PathFollow2D.new() as PathFollow2D
	trail_offset = PathFollow2D.new() as PathFollow2D
	portal_offset = PathFollow2D.new() as PathFollow2D
	follow.rotate = false
	follow.cubic_interp = true
	trail_offset.rotate = false
	trail_offset.cubic_interp = true
	_loop.add_child(follow)
	_loop.add_child(trail_offset)
	_loop.add_child(portal_offset)
	trail = trail_s.instance()
	trail.amount = trail_speed * 1.5
#	trail_offset.add_child(trail)
	add_child(trail)
	Game.power = 100
	Game.score = 0
	
	Game.triggers = 0
	
	var _triggers = level_scene.get_children()
	for z in _triggers:
		print(z.name)
		if (z.name.findn("trigger") >= 0):
			Game.triggers += 1
	
	_hide_ui()
	ui_game.visible = true
	
	is_enabled = false
	
	$UI/Level/NinePatchRect2/Label.bbcode_text = "\n[center]LEVEL {0}[/center]".format([Game.level])
	ui_level.visible = true

	var timer = Timer.new()
	timer.wait_time = 1.0
	timer.one_shot = true
	timer.connect("timeout", self, "_start_level")
	add_child(timer)
	timer.start()
	

func _process(delta):
	
	if (Input.is_action_just_pressed("pause") || resume_game):
		resume_game = false
		is_paused = ! is_paused
		is_enabled = ! is_paused
		$UI/Pause.visible = is_paused
		$UI/Pause/VBoxContainer/ContinueButton.grab_focus()
		
		if (is_paused):
			Game.emit_signal("pause")
			time_end = OS.get_ticks_msec()
			time_level += (time_end - time_start)
		else:
			Game.emit_signal("resume")
			time_start = OS.get_ticks_msec()
		
	if (is_paused):
		return
		
	
	trail_location += trail_speed * 10 * delta
	trail_offset.offset = trail_location
	trail.global_position = trail_offset.global_position
	
	if (Game.debug):
		var _debug = ""
		_debug += "power : {0}\n".format([Game.power])
		_debug += "bonus : {0}\n".format([Game.score])
		_debug += "triggers : {0}\n".format([Game.triggers])
		_debug += "level : {0}\n".format([Game.level])
		_debug += "time : {0}\n".format([time_level])
		_debug += "overall : {0}\n".format([Game.time])
		$"UI/Game/$Debug".text = _debug
	
	rot_speed = player_rotation_speed * 5 * delta
	
	if (laser):
		if (laser.is_casting):
			rot_speed = player_rotation_variance * 5 * delta
			$Camera2D/ScreenShake.start(0.1, 15, 10, 0)
		
	if (! is_enabled):
		return

	player_location += player_speed * 10 * delta
	follow.offset = player_location
	player.global_position = follow.global_position

	player.rotation += rot_speed * 5 * delta
	
		

func _on_hit_trigger(count):
	
	if (! is_enabled):
		return
		
	Game.triggers += count
	
	if (Game.triggers < 1):
		_end_level()
		var _ms_game = "%5.2f" % [Game.time / 1000.0]
		var _ms_level = "%5.2f" % [time_level / 1000.0]
		var _message = "\n[center][shake]NICE![/shake][/center]"
		var _stats = "\n[center]LEVEL TIME {0} sec\n\nOVERALL TIME {1} sec[/center]"
		$UI/Win/WinMessage.bbcode_text = _message
		$UI/Win/WinStats.bbcode_text = _stats.format([_ms_level, _ms_game])
		ui_win.visible = true
		audio_win.play()
		var timer = Timer.new()
		timer.wait_time = 3.5
		timer.one_shot = true
		timer.connect("timeout", self, "_next_level")
		add_child(timer)
		timer.start()
		
		
	
func _on_no_power():
	Logger.trace("[GameWork] _on_no_power")
	
	if (! is_enabled):
		return
		
	_end_level()
	ui_lose.visible = true
	$UI/Lose/LoseMessage.bbcode_text = "\n[center]no power left[/center]"
	audio_lose.play()


func _hide_ui():
	ui_game.visible = false
	ui_lose.visible = false
	ui_win.visible = false
	

func _on_ContinueYesButton_pressed():
	Game.continued = true
	SceneManager.transition_to(Ludum.settings.play_scene)


func _on_ContinueNoButton_pressed():
	Game.level = 1
	Game.time = 0
	SceneManager.transition_to(Ludum.settings.home_scene)


func _end_level():
	Logger.trace("[GameWork] _end_level")
	time_end = OS.get_ticks_msec()
	time_level += (time_end - time_start)
	Game.time += time_level
	_hide_ui()
	_hide_player()
	is_enabled = false
	player.rotation = 0
	audio_game.stop()
	_stop_laser()
#	var timer = Timer.new()
#	timer.wait_time = 1.5
#	timer.one_shot = true
#	timer.connect("timeout", self, "_stop_laser")
#	add_child(timer)
#	timer.start()


func _next_level():
	
	Game.level += 1	
	
	if (Game.level > Game.levels.size() - 1):
		Game.level = 1
		SceneManager.transition_to("res://game/scenes/win.tscn")
	else:	
		SceneManager.transition_to(Ludum.settings.play_scene)


func _on_hit_player():
	
	$Camera2D/ScreenShake.start(0.5, 12, 5, 1)
	audio_crash.play()
	var _dead = player_dead_s.instance()
	add_child(_dead)
	_dead.global_position = player.global_position	
	_dead.rotation_degrees = player.rotation_degrees
	_on_no_power()



func _start_level():
	player = player_s.instance()
	add_child(player)
	laser = player.find_node("Laser")
	rot_speed = player_rotation_speed
	portal = portal_s.instance()
	
	time_start = OS.get_ticks_msec()
	
	is_enabled = true	

	ui_level.visible = false


func _hide_player():
	player.global_position = Vector2.ZERO
	player.visible = false


func _stop_laser():
	laser.is_casting = false


func _on_ContinueButton_pressed():
	resume_game = true


func _on_RestartButton_pressed():
	Game.time += time_level
	SceneManager.transition_to(Ludum.settings.play_scene)


func _on_HomeButton_pressed():
	SceneManager.transition_to(Ludum.settings.home_scene)
