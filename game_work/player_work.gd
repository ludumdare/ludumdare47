extends Area2D

onready var emitter = $Particles2D as Particles2D



func _on_Player_area_entered(area):
	print(area.name)
	if (area.name.findn("trigger") >= 0):
		if (area.is_active):
			Game.emit_signal("hit_player")
