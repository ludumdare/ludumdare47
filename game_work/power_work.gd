extends Control

onready var line = get_node("Line") as Line2D

func _process(delta):
	
	var _remaining : float = Game.power / 100.0
	line.points[1].x = _remaining * 500
	
	line.default_color = Color.blue
	
	if (Game.power < 50):
		line.default_color = Color.yellow
	
	if (Game.power < 25):
		line.default_color = Color.red
	
