extends Area2D

var is_active = true
var is_enabled = true
var timer

onready var audio_up : AudioStreamPlayer2D = $Up
onready var audio_down : AudioStreamPlayer2D = $Down

func hit():
	
	if (!is_enabled):
		return
		
	if (is_active):
		Game.emit_signal("hit_trigger", -1)
		audio_up.play()
	else:
		Game.emit_signal("hit_trigger", +1)
		audio_down.play()
		
	is_active = !is_active
	
	$Sprite1.visible = false
	$Sprite2.visible = false
	
	if (is_active):
		$Sprite1.visible = true
	else:
		$Sprite2.visible = true
	
	
	is_enabled = false
	
	timer = Timer.new()
	timer.wait_time = 1.0
	timer.one_shot = true
	timer.connect("timeout", self, "_enable")
	add_child(timer)
	timer.start()
	
	
func _enable():
	timer.queue_free()
	is_enabled = true
