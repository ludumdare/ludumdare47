extends Node2D

onready var s_player = preload("res://game_work/player_work.tscn")


export var speed : float = .001

var path_location = 0
var player
var follow

func _process(delta):
	follow.unit_offset = path_location
	path_location += speed

func _ready():
	follow = PathFollow2D.new()
	$Level1.add_child(follow)
	player = s_player.instance()
	follow.add_child(player)

