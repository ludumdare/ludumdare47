extends Path2D

export var speed : float = 5.0

onready var trigger_s = preload("res://game_work/trigger_work.tscn")

var trigger
var trigger_follow
var trigger_location = 0


var is_enabled = true


func _ready():
	
	trigger = trigger_s.instance()
	get_parent().get_parent().add_child(trigger)
	trigger_follow = PathFollow2D.new() as PathFollow2D
	trigger_follow.rotate = false
	trigger_follow.cubic_interp = true
	add_child(trigger_follow)
	
	Game.connect("pause", self, "_on_pause")
	Game.connect("resume", self, "_on_resume")
	

func _process(delta):
	
	if (! is_enabled):
		return
	
	trigger_location += speed * 10 * delta
	trigger_follow.offset = trigger_location
	trigger.global_position = trigger_follow.global_position	
	
	
func _on_pause():
	is_enabled = false

	
func _on_resume():
	is_enabled = true
