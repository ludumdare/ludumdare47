# PILOT

Your Ship is **stuck in an endless loop** and you are spinning out of control. shoot the targets to escape each level, but only shoot them once.

How **fast** can you finish each level? 
How **fast** can you finish the entire game? 

See your **NAME**  on the scores list for those **AWESOME PLAYERS** who are the fastest.

!! GOOD LUCK !!

## CONTROLS

#### SHOOT
left mouse button

#### ESCAPE
pause game

## TIME
The game keeps track of how long it takes you to complete a level and how long it takes you to complete the entire game. My hope is that we will see some speed runs of the game at some point.

If you fail a level and continue, or have to restart a level, the time you played is still accumulated for your overall time playing.

# HAVE FUN
